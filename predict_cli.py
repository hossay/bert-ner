import sys
import time
import numpy as np
from ner_predict import prepare_model, PredictionWrapper
import nltk
import re

from utils import *


if __name__ == '__main__':

    print('Please wait while the Fine-Tuned BERT model is loaded. ')
    processor, label_list, tokenizer, estimator = prepare_model()
    print('*** Model is successfully loaded ***')
    print(label_list)

    model = PredictionWrapper(processor, labels, tokenizer, estimator)
    while True:
        sentence = input('Please enter a sentence to detect: \n')
        if sentence.lower() in ['cancel', 'terminate', 'exit']:
            sys.exit()

        tokens = prepare_predict_file(sentence)
        print('You have entered: {}'.format(sentence))
        start_time = time.time()
        for _ in range(2):
          result = model.predict()[0]

        print(result)

        # NER tags
        ner_tags = get_ner_tags(result, tokens)
        print(ner_tags)

        # # POS tags
        # pos_tags = nltk.pos_tag(tokens)
        #
        # ne_words = filter(lambda x: x[1].startswith('B') or x[1].startswith('I'), ner_tags)
        # noun_words = filter(lambda x: x[1].startswith('N'), pos_tags)
        #
        # space_sep = sentence.split()
        # months = ['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december',
        #           'jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec']
        #
        # p = re.compile(r"\d{1,2}(st|nd|rd|th)")
        #
        # date_words = filter(lambda x: x in months,)
        #
        # # ne_set + noun_set + date_set
        # target_set = ne_set.union(noun_set)
        #
        # print(pos_tags)

        print('***** Prediciton takes {} seconds.'.format(str(round(time.time() - start_time, 2))))
