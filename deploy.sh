#!/usr/bin/env bash
nvidia-docker stop ner_container
nvidia-docker rm ner_container
nvidia-docker build --network host -t ner_image .
nvidia-docker run -d -p 60053:50000 --name ner_container ner_image