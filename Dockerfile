FROM ubuntu:16.04
FROM nvidia/cuda:9.0-cudnn7-devel

MAINTAINER <hossay> <<youhs4554@gmail.com>>

# 기본 패키지들 설치 및 Python 3.5 설치
RUN apt-get update
RUN apt-get install -y software-properties-common
RUN add-apt-repository -y ppa:fkrull/deadsnakes
RUN apt-get update
RUN apt-get install -y --no-install-recommends python3.5 python3.5-dev python3-pip python3-setuptools python3-wheel gcc
RUN apt-get install -y git

# pip 업그레이드
RUN python3.5 -m pip install pip --upgrade

# 현재 디렉토리의 모든 파일들을 도커 컨테이너의 /python-docker 디렉토리로 복사 (원하는 디렉토리로 설정해도 됨)
ADD . /python-docker

# 작업 디렉토리로 이동
WORKDIR /python-docker

# 작업 디렉토리에 있는 requirements.txt로 패키지 설치
RUN pip3 install -r requirements.txt

# nltk_data
RUN python3.5 -m nltk.downloader -d /nltk_data punkt

# 컨테이너에서 실행될 명령어. 컨테이거나 실행되면 app.py를 실행시킨다.
CMD python3.5 app.py

EXPOSE 50000
