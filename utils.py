from nltk.tokenize import word_tokenize

predict_file_path = './NERdata/to_predict.txt'
labels = ['NaN', 'B-MISC', 'I-MISC', 'O', 'B-PER', 'I-PER', 'B-ORG', 'I-ORG', 'B-LOC', 'I-LOC', 'X']

def prepare_predict_file(sentence):
    tokens = word_tokenize(sentence)
    # end sentence with dot
    if tokens[-1] != '.':
        tokens.append('.')

    with open(predict_file_path, 'w') as f:
        # Write file header
        f.write('-DOCSTART- -X- -X- O\n')
        f.write('\n')

        for t in tokens:
            f.write('{} O O O\n'.format(t))

        f.write('\n')

    return tokens


def get_ner_tags(result, tokens):
    ner_tags = []
    for k, v in enumerate(result):
        if v != 0 and k < len(tokens):
            ner_tags.append((tokens[k-1],labels[v]))

    return ner_tags
