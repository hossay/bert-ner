from flask import Flask, request
from utils import *
from ner_predict import prepare_model, PredictionWrapper
import nltk
import json
from collections import OrderedDict

app = Flask(__name__)

# init model
print('Please wait while the Fine-Tuned BERT model is loaded. ')
processor, label_list, tokenizer, estimator = prepare_model()
print('*** Model is successfully loaded ***')

model = PredictionWrapper(processor, labels, tokenizer, estimator)

@app.route("/")
def hello():
    return "Hello World!"

@app.route("/NER")
def ner_middle_ware():
    sentence = request.args.get('sentence').lower()

    tokens = prepare_predict_file(sentence)
    print('You have entered: {}'.format(sentence))
    for _ in range(2):
      result = model.predict()[0]

    # NER tags
    NER_tags = get_ner_tags(result, tokens)

    entity_groups = {}; ptr = 0
    for word, tag in NER_tags:
        if tag.startswith('B'):
            entity_groups[ptr] = [word]
        elif tag.startswith('I'):
            if not entity_groups.get(ptr):
                continue
            entity_groups[ptr] += [word]
        else:
            ptr += 1

    # sort according ordering
    sorted_items = sorted(entity_groups.items(),key=lambda x: x[0])

    entity_words = list(
        map(lambda x: ' '.join(x[1]), sorted_items)
    )

    print(NER_tags)

    return json.dumps({
        'entity_words': entity_words,
    })

app.run(host='0.0.0.0', port=50000)